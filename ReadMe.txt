This is a personal academic project done by me under a professor.
It was done to explore the usage a capability of DynamoDB from Amazon Web Services (AWS).
Technologies Used:
    Front-end : HTML/CSS-Bootstrap
    Back-end : Python-Flask framework
    Database : DynamoDB (Cloud)

PLEASE DO OPEN THE 'project_output.pdf' WHICH SHOWS THE EXACT LOOK AND FEEL
OF THE WEBSITE CREATED.
It also shows various snapshots of how the data was stored and accessed using the back-end framework.