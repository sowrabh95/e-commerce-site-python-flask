
from flask import Flask,redirect,url_for,make_response,session,request,render_template
import functions as func
import boto3
import uuid
import datetime
from random import randint

app = Flask(__name__)
app.config['SECRET_KEY'] = 'sowrabh'
dynamodb = boto3.resource('dynamodb')
client = boto3.client('dynamodb')
cart = {}
orderidlist = []
viewedproducts = []

@app.route('/',methods=['POST','GET'])
def login():
    if request.method == 'POST':
        username = request.form.get('uname')
        password = request.form.get('psw')
        if username == '' or password == '':
            return render_template('login.html',message='Wrong Id or password')
        userstable = dynamodb.Table('Users')
        res = userstable.get_item(Key={'User_ID': str(username)})
        if 'Item' in res:
            if password == res['Item']['Password']:
                session['email'] = res['Item']['User_ID']
                session['uname'] = res['Item']['UserName']
                session['uid'] = uuid.uuid1()
                session['addedtodb'] = False
                today = datetime.datetime.now()
                session['datetime'] = today
                session['cart'] = cart
                session['authenticated'] = True
                return redirect(url_for('Profile'))
        else:
            errormess = 'Wrong Id or password'
            return render_template('login.html',message=errormess)
    else:
        return render_template('login.html')

@app.route('/Profile',methods=['POST','GET'])
def Profile():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    res = client.get_item(TableName='Viewed_Products',Key = {'email':{'S':session['email']}})
    plist = []
    if 'Item' in res:
        for item in res['Item']['products']['L']:
            plist.append(item['N'])
    if len(plist) > 0:
        return render_template('profile.html',User_Name=session['uname'],plist=plist)
    else:
        return render_template('profile.html',User_Name=session['uname'])

@app.route('/Flashsale',methods=['POST','GET'])
def Flashsale():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    randnums = []
    offerlist = [.25,.50,.75]
    i = 0
    while i < 3:
        randnum = randint(1,14)
        if randnum in randnums:
            continue
        else:
            randnums.append(randnum)
            i = i + 1
    productlisinfo = []
    for num in randnums:
        product_res = client.get_item(TableName='Products',Key={'Product_ID':{'N':str(num)}})['Item']
        product_name = product_res['Product_Name']['S']
        price = int(product_res['Price']['N'])
        offer = offerlist[randint(0,2)]*100
        offerprice = (offer/100)*price
        productlisinfo.append((num,product_name,price,offerprice,str(offer)))
    return render_template('flashsale.html',productlisinfo=productlisinfo,User_Name=session['uname'])

@app.route('/Products',methods=['POST','GET'])
def Products():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    productlis = func.getproducts()
    return render_template('products.html',User_Name=session['uname'],productlis=productlis)

@app.route('/Orders',methods=['POST','GET'])
def Orders():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    res = client.scan(TableName='Orders', AttributesToGet=['Product_Name', 'UnitPrice', 'Quantity', 'Date','Order_ID','street','state','city','zipcode'],
                      ScanFilter={'User_ID': {'AttributeValueList': [{'S': session['email']}],'ComparisonOperator': 'EQ'}})
    orderlist = []
    for item in res['Items']:
        tempdict = {}
        tempdict['Date'] = item['Date']['S']
        tempdict['Product_Name'] = item['Product_Name']['S']
        tempdict['UnitPrice'] = item['UnitPrice']['N']
        tempdict['Quantity'] = item['Quantity']['N']
        tempdict['Order_ID'] = item['Order_ID']['N']
        tempdict['Delivery_address'] = item['street']['S'] + "," + item['state']['S'] + "," + item['city']['S'] + "," + item['zipcode']['S']
        orderlist.append(tempdict)
    return render_template('orders.html',User_Name=session['uname'],orderlist=orderlist)

@app.route('/Signup',methods=['POST','GET'])
def Signup():
    if request.method == 'POST':
        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')
        retypepass = request.form.get('retypepassword')
        retval = func.signupvalidation(username,email,password,retypepass)
        if retval == 4:
            return render_template('signup.html',message='one of the inputs is blank')
        elif retval == 1:
            return render_template('signup.html',message='email format wrong')
        elif retval == 2:
            return render_template('signup.html',message='account with this email already exists')
        elif retval == 3:
            return render_template('signup.html',message='passwords don\'t match')
        elif retval == 0:
            try:
                client.put_item(TableName='Users',Item={'User_ID': {'S': email}, 'Password': {'S': password}, 'UserName': {'S':username.strip()}})
                client.put_item(TableName='maxorder',Item={'email':{'S':email},'maxorderval':{'N':'1'}})
            except Exception as e:
                return render_template('signup.html',message=str(e)+" db error")
            return redirect(url_for('login'))
        else:
            return render_template('signup.html',message='some error!')
    return render_template('signup.html')

@app.route('/Cart',methods=['POST',"GET"])
def Cart():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    shoppingcart = func.getcartproducts(cart)
    return render_template('cart.html',shoppingcart=shoppingcart,User_Name=session['uname'])


@app.route('/checkout',methods=['POST','GET'])
def checkout():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    if request.method == 'POST':
        addressdict = {}
        addressdict['street'] = request.form.get('ad1') + " " + request.form.get('ad2')
        addressdict['state'] = request.form.get('state')
        addressdict['city'] = request.form.get('city')
        addressdict['zipcode'] = request.form.get('zipcode')
        maxorderval = client.get_item(TableName='maxorder', Key={'email': {'S': session['email']}})
        currentorderval = int(maxorderval['Item']['maxorderval']['N'])
        cartlis = func.getcartproducts(cart)
        for product in cartlis:
            currentorderval = currentorderval + 1
            client.put_item(TableName='Orders',
                            Item={'User_ID':{'S':session['email']},'Order_ID':{'N':str(currentorderval)},
                                  'Category_ID':{'N':str(product['Category_ID'])},'Date':{'S':str(session['datetime'])},
                                  'Product_ID':{'N':str(product['Product_ID'])},'Product_Name':{'S':product['Product_Name']},
                                  'Quantity':{'N':str(product['quantity'])},'UnitPrice':{'N':str(product['Price'])},
                                  'street':{'S':addressdict['street']},'state':{'S':addressdict['state']},'city':{'S':addressdict['city']},
                                  'zipcode':{'S':addressdict['zipcode']}})
            orderidlist.append(currentorderval)
        client.update_item(TableName='maxorder', Key={'email': {'S': session['email']}},
                           AttributeUpdates={'maxorderval': {'Value': {'N': str(currentorderval)}}})
        return redirect(url_for('bought'))
    else:
        return render_template('checkout.html',User_Name=session['uname'])

@app.route('/bought',methods=['POST','GET'])
def bought():
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    return render_template('bought.html',User_Name=session['uname'])

@app.route('/removeincart/<productid>')
def removeincart(productid):
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    print(cart)
    del cart[productid]
    print(cart)
    return redirect(url_for('Cart'))

@app.route('/ProductPage/<productid>',methods=['POST','GET'])
def ProductPage(productid):
    if len(viewedproducts) >= 3 and productid not in viewedproducts:
        viewedproducts.pop(0)
        viewedproducts.append(productid)
    elif productid not in viewedproducts:
        viewedproducts.append(productid)
    if 'authenticated' not in session.keys():
        return redirect(url_for('login'))
    pinfo = func.loadproductinfo(productid)
    if request.method == 'POST':
        currentquan = int(client.get_item(TableName='Products',Key={'Product_ID':{'N':productid}})['Item']['Quantity']['N'])
        userquan = request.form.get('quan')
        if userquan.isdigit() == False:
            return render_template('productpage.html',productid=productid,productinfo=pinfo,User_Name=session['uname'],
                                    message='enter proper quantity')
        userquan = int(userquan)
        if currentquan > userquan:
            if str(productid) not in cart:
                cart[productid] = userquan
            else:
                cart[productid] += userquan
            session['cart'] = cart
            print(session['cart'])
            return redirect(url_for('ProductPage',productid=productid,productinfo=pinfo,User_Name=session['uname']))
        else:
            return render_template('productpage.html',productid=productid,message='Not enough stock',productinfo=pinfo,
                                   User_Name=session['uname'])
    else:
        if type(pinfo) == str():
            return render_template('prductpage.html',productid=productid,message=pinfo,User_Name=session['uname'])
        return render_template('productpage.html',productid=productid,productinfo=pinfo,User_Name=session['uname'])

@app.route('/logout',methods=['POST','GET'])
def logout():
    temp_productlis = []
    for item in viewedproducts:
        temp_productlis.append({'N':item})
    endtime = datetime.datetime.now()
    del session['authenticated']
    temporderlis = []
    for orderid in orderidlist:
        temporderlis.append(str(orderid))
    if len(temporderlis) != 0:
        client.put_item(TableName='sessions',Item={'email':{'S':session['email']},'session_id':{'S':str(session['uid'])},
                                               'start DateTime':{'S':str(session['datetime'])},'end Datetime':{'S':str(endtime)},
                        'orderlist':{'NS':temporderlis}})
        if len(temp_productlis) != 0:
            print('executed')
            client.put_item(TableName='Viewed_Products',Item={'email':{'S':session['email']},'products':{'L':temp_productlis}})
    else:
        client.put_item(TableName='sessions',
                        Item={'email': {'S': session['email']}, 'session_id': {'S': str(session['uid'])},
                              'start DateTime': {'S': str(session['datetime'])},'end Datetime':{'S':str(endtime)},
                              'orderlist':{'S':'None'}})
        client.put_item(TableName='Viewed_Products',
                        Item={'email': {'S': session['email']}, 'products': {'L': temp_productlis}})
        if len(temp_productlis) != 0:
            print('executed')
            client.put_item(TableName='Viewed_Products',Item={'email':{'S':session['email']},'products':{'L':temp_productlis}})

    return redirect(url_for('login'))

app.run(debug=True)