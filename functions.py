import re
import boto3


dynamodb = boto3.resource('dynamodb')
client = boto3.client('dynamodb')

def main():
    pass

def signupvalidation(username,email,password,retypepass):
    emailbool = False
    passwordbool = False
    usernamebool = False
    if username == '' or email == '' or password == '' or retypepass == '':
        return 4
    if username != '':
        usernamebool = True
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if not re.search(regex,email):
        return 1
    emailres = client.get_item(TableName='Users',Key={'User_ID':{'S':email}})
    if 'Item' not in emailres:
        emailbool = True
    else:
        return 2
    if password != retypepass:
        return 3
    else:
        passwordbool = True
    if passwordbool == True and emailbool==True and usernamebool == True:
        return 0
    else:
        return 5


def loadproductinfo(productid):
    response = client.get_item(TableName='Products', Key={'Product_ID': {'N': productid}})
    attrlist = [('Product_ID','N'),('Product_Name','S'),('Price','N'),('Quantity','N'),('Product_Desc','S')]
    tempdict = {}
    if 'Item' in response:
        for attr in attrlist:
            tempdict[attr[0]] = response['Item'][attr[0]][attr[1]]
        return tempdict
    else:
        return 'No Item'

def getcartproducts(cart):
    templis = []
    for key in cart:
        tempdict = {}
        pres = client.get_item(TableName='Products',Key={'Product_ID':{'N':key}})
        tempdict['Product_Name'] = pres['Item']['Product_Name']['S']
        tempdict['Price'] = pres['Item']['Price']['N']
        tempdict['quantity'] = cart[key]
        tempdict['Product_ID'] = pres['Item']['Product_ID']['N']
        tempdict['Category_ID'] = pres['Item']['Category_ID']['N']
        templis.append(tempdict)
    return templis


def getproducts():
    catdict = {}
    res = client.scan(TableName='Category')
    for item in res['Items']:
        catdict[item['Category_ID']['N']] = item['Category_Name']['S']
    productlis = []
    res = client.scan(TableName='Products')
    for item in client.scan(TableName='Products')['Items']:
        tempdict = {}
        tempdict['product_id'] = item['Product_ID']['N']
        tempdict['cat_name'] = catdict[item['Category_ID']['N']]
        tempdict['price'] = item['Price']['N']
        tempdict['product_name'] = item['Product_Name']['S']
        tempdict['quantity'] = item['Quantity']['N']
        productlis.append(tempdict)
    return productlis



if __name__ == '__main__':
    main()